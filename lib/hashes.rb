# require 'byebug'

# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash = Hash.new
  str.split.each { |word| hash[word] = word.length }
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.chars.each { |char| hash[char] += 1 }
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  answer = []
  hash = Hash.new(0)
  arr.each { |num| hash[num] += 1 }
  hash.each { |k, v| answer << k }
  answer
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)
  numbers.each { |num| num.even? ? hash[:even] += 1 : hash[:odd] += 1 }
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash = Hash.new(0)
  vows = %w[a e i o u]
  answer = nil
  count = 0
  string.chars { |char| hash[char] += 1 if vows.include?(char) }
  hash = hash.sort_by { |k, v| k }
  hash.each { |k, v| (answer = k) && (count = v) if v > count }
  answer
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  answer = []
  bdays = []
  students.each { |k, v| bdays << k if v > 6 }
  bdays.each_with_index do |bday, idx|
    idx2 = idx + 1
    while idx2 < bdays.length
      answer << [bdays[idx], bdays[idx2]]
      idx2 += 1
    end
  end
  answer
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hash = Hash.new(0)
  specimens.each { |specie| hash[specie] += 1 }
  hash = hash.sort_by { |k, v| v }
  (hash.length**2) * hash[0][1] / hash[-1][1]
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  norm_hash = Hash.new(0)
  van_hash = Hash.new(0)
  character_count(normal_sign).each { |let| norm_hash[let] += 1 }
  character_count(vandalized_sign).each { |let| van_hash[let] += 1 }
  van_hash.each do |k, v|
    return false if v > norm_hash[k]
  end
  true
end

def character_count(str)
  array = []
  alpha = ('a'..'z').to_a
  word = str.downcase
  word.chars { |char| array << char if alpha.include?(char) }
  array
end
